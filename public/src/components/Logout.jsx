import React from 'react'
import {useNavigate} from 'react-router-dom'
import styled from 'styled-components'
import {GenIcon} from 'react-icons'
// import {MessageBox} from 'element-react'
// import 'element-theme-default'
export default function Logout() {
    const navigate=useNavigate()
    const handleClick=()=>{
        async function fecthData(){
          // MessageBox.$confirm('此操作将退出该网站, 是否继续?', '提示', {
          //   confirmButtonText: '确定',
          //   cancelButtonText: '取消',
          //   type: 'warning',
          //   center: true
          // }).then(() => {
          //   this.$message({
          //     type: 'success',
          //     message: '退出成功!'
          //   })
          // }).catch(() => {
          //   this.$message({
          //     type: 'info',
          //     message: '已取消退出'
          //   })
          // })
          localStorage.clear()
          navigate("/login")
        }
        fecthData()
    }
  return (
    <Button onClick={handleClick}>
        <GenIcon/>
    </Button>
  )
}
const Button=styled.button`
display: flex;
justify-content: center;
align-items: center;
padding: 0.5rem;
border-radius: 0.5rem;
background-color: #9a86f3;
border: none;
cursor: pointer;
svg {
  font-size: 1.3rem;
  color: #ebe7ff;
}
`
